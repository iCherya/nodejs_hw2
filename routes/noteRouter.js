const express = require('express');
const router = new express.Router();

const {BadRequestError} = require('../controllers/errorController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {
  getAllNotes,
  createNote,
  findNote,
  getNote,
  updateNote,
  toggleNoteCompletion,
  deleteNote,
} = require('../controllers/noteController');

router.use(authMiddleware);

router.get('/', getAllNotes);
router.post('/', createNote);

router.use('/:id', findNote);

router.get('/:id', getNote);
router.put('/:id', updateNote);
router.patch('/:id', toggleNoteCompletion);
router.delete('/:id', deleteNote);

router.use((req, res, next) => {
  next(new BadRequestError());
});

module.exports = router;
