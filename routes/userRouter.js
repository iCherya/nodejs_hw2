const express = require('express');
const router = new express.Router();

const {BadRequestError} = require('../controllers/errorController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {
  getUserInfo,
  deleteUser,
  changeUserPassword,
} = require('../controllers/userController');

router.use('/', authMiddleware);

router.get('/', getUserInfo);
router.patch('/', changeUserPassword);
router.delete('/', deleteUser);

router.use((req, res, next) => {
  next(new BadRequestError());
});

module.exports = router;
