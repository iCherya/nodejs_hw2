const express = require('express');
const router = new express.Router();

const {BadRequestError} = require('../controllers/errorController');
const {validate, register, login} = require('../controllers/authController');

router.use('/', validate);

router.post('/register', register);
router.post('/login', login);

router.use((req, res, next) => {
  next(new BadRequestError());
});

module.exports = router;
