const jwt = require('jsonwebtoken');
const config = require('config');

const {BadRequestError} = require('../../controllers/errorController');

const authMiddleware = async (req, res, next) => {
  const authData = req.headers.authorization;

  if (!authData) {
    next(new BadRequestError(`No Authorization http header found!`));
  }

  const [, token] = authData.split(' ');

  if (!token) {
    next(new BadRequestError(`No JWT token found!`));
  }

  try {
    const decoded = jwt.verify(token, config.get('JWT_SECRET'));
    req.user = decoded;
    next();
  } catch (error) {
    next(new BadRequestError(`Unauthorized`));
  }
};

module.exports = {authMiddleware};
