const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const config = require('config');

const authRouter = require('./routes/authRouter');
const notesRouter = require('./routes/noteRouter');
const usersRouter = require('./routes/userRouter');

const app = express();
const port = process.env.PORT || 8080;

app.use(express.json());
app.use(morgan('combined'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/notes', notesRouter);

app.use((err, req, res, next) => {
  res.status(err.statusCode).json({message: err.message});
});

app.use((req, res) => {
  res.status(404).json({message: 'Not found'});
});

const start = async () => {
  const {DB_URL, DB_PARAMS} = config.get('DB');
  await mongoose.connect(DB_URL, DB_PARAMS);

  app.listen(port, () => {
    console.log(`🚀 Server started at port ${port}`);
  });
};

start();
