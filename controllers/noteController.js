const {
  BadRequestError,
  ServerError,
} = require('../controllers/errorController');
const {Note} = require('../models/noteModel');

module.exports.findNote = async (req, res, next) => {
  try {
    const note = await Note.findOne({
      _id: req.params.id,
      userId: req.user._id,
    }).select('-__v');

    req.note = note;
    next();
  } catch (error) {
    return next(new BadRequestError('Note with requested ID not found'));
  }
};
module.exports.createNote = async (req, res, next) => {
  const newNote = new Note({
    text: req.body.text,
    userId: req.user._id,
  });

  try {
    await newNote.save();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    return next(new ServerError(error.message));
  }
};
module.exports.getAllNotes = async (req, res, next) => {
  const {offset = 0, limit = 50} = req.query;
  const {_id} = req.user;

  const requestOptions = {
    skip: parseInt(offset),
    limit: parseInt(limit),
  };

  try {
    const notes = await Note.find({userId: _id}, {__v: 0}, requestOptions);

    res.status(200).json({notes});
  } catch (error) {
    return next(new ServerError(error.message));
  }
};
module.exports.getNote = async (req, res, next) => {
  res.status(200).json({note: req.note});
};
module.exports.updateNote = async (req, res, next) => {
  const note = req.note;
  const oldNoteText = note.text;
  const newNoteText = req.body.text;

  if (!newNoteText) {
    return next(new BadRequestError('Provide text for note'));
  }

  if (newNoteText === oldNoteText) {
    return next(new BadRequestError('Note content did not changed'));
  }

  note.text = newNoteText;

  try {
    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    return next(new ServerError(error.message));
  }
};
module.exports.toggleNoteCompletion = async (req, res, next) => {
  const note = req.note;

  note.completed = !note.completed;

  try {
    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    return next(new ServerError(error.message));
  }
};
module.exports.deleteNote = async (req, res, next) => {
  const note = req.note;

  try {
    await Note.findOneAndDelete({
      _id: note._id,
      userId: req.user._id,
    });
    res.status(200).json({message: 'Success'});
  } catch (error) {
    return next(new ServerError(error.message));
  }
};
