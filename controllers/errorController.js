/**
 * Class representing custom BadRequestError
 */
class BadRequestError extends Error {
  /**
   * Create error
   * @param {string} message
   */
  constructor(message = 'Bad request') {
    super(message);
    this.name = 'BadRequestError';
    this.statusCode = 400;
  }
}

/**
 * Class representing custom ServerError
 */
class ServerError extends Error {
  /**
   * Create error
   * @param {string} message
   */
  constructor(message = 'Internal server error') {
    super(message);
    this.name = 'ServerError';
    this.statusCode = 500;
  }
}

module.exports = {BadRequestError, ServerError};
