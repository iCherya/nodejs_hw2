const config = require('config');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Joi = require('joi');

const {BadRequestError} = require('./errorController');
const {User} = require('../models/userModel');

module.exports.validate = async (req, res, next) => {
  const {username, password} = req.body;

  if (!username || !password) {
    return next(new BadRequestError('Missed credenticals'));
  }

  const schema = Joi.object({
    username: Joi.string().alphanum().required(),
    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  try {
    await schema.validateAsync(req.body);
  } catch (error) {
    return next(new BadRequestError(error.message));
  }

  next();
};
module.exports.register = async (req, res, next) => {
  const {username, password} = req.body;

  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });

  try {
    await user.save();
  } catch (error) {
    if (error.code === 11000) {
      return next(new BadRequestError('User is already registered'));
    }

    return next(new BadRequestError(e.message));
  }

  res.status(200).json({message: 'Success'});
};
module.exports.login = async (req, res, next) => {
  const {username, password} = req.body;

  const user = await User.findOne({username});

  if (!user) {
    return next(new BadRequestError(`User is not registered`));
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return next(new BadRequestError(`Wrong password!`));
  }

  const JWT_SECRET = config.get('JWT_SECRET');
  const token = jwt.sign({username: user.username, _id: user._id}, JWT_SECRET);

  res.status(200).json({message: 'Success', jwt_token: token});
};
module.exports.auth = async (req, res) => {
  const header = req.headers.authorization;

  if (!header) {
    next(new BadRequestError(`No Authorization http header found!`));
  }

  const [, token] = header.split(' ');

  if (!token) {
    next(new BadRequestError(`No JWT token found!`));
  }

  const JWT_SECRET = config.get('JWT_SECRET');
  const decoded = jwt.verify(token, JWT_SECRET);

  const user = await User.findOne({
    _id: decoded._id,
  });

  if (!user) {
    next(new BadRequestError(`Please authenticate!`));
  }

  req.user = user;
  next();
};
