const Joi = require('joi');
const bcrypt = require('bcrypt');

const {
  BadRequestError,
  ServerError,
} = require('../controllers/errorController');
const {User} = require('../models/userModel');
const {Note} = require('../models/noteModel');

module.exports.getUserInfo = async (req, res) => {
  const {_id} = req.user;

  try {
    const user = await User.findOne({_id: _id}, {createdDate: 1, username: 1});

    if (!user) {
      return new BadRequestError();
    }

    res.status(200).json({user: user});
  } catch (error) {
    return new ServerError(error.message);
  }
};
module.exports.changeUserPassword = async (req, res, next) => {
  const {oldPassword, newPassword} = req.body;

  const schema = Joi.object({
    oldPassword: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    newPassword: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  try {
    await schema.validateAsync({newPassword});
  } catch (error) {
    return next(new BadRequestError(error.message));
  }

  try {
    const user = await User.findById(req.user._id);
    const passwordsMatch = await bcrypt.compare(oldPassword, user.password);

    if (!passwordsMatch) {
      return next(new BadRequestError(`Incorrect password`));
    }

    const newPasswordHashed = await bcrypt.hash(newPassword, 10);
    user.password = newPasswordHashed;
    await user.save();

    res.status(200).json({message: 'Success'});
  } catch (error) {
    return next(new ServerError(error.message));
  }
};
module.exports.deleteUser = async (req, res, next) => {
  const {_id} = req.user;

  try {
    const user = await User.findOne({_id: _id});
    const notes = await Note.find({userId: user._id});

    if (notes.length) {
      for (const note of notes) {
        await note.remove();
      }
    }

    await user.remove();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    return next(new ServerError(error.message));
  }
};
